PKSOverlay.prototype = new google.maps.OverlayView();

function PKSOverlay(bounds, map, imgs) {
    this.bounds_ = bounds;
    this.imgs_ =  imgs;
    this.map_ = map;

    this.div_ = null;
    this.setMap(map);
}


PKSOverlay.prototype.onAdd = function() {
  // Creating a container div
  var overlayDiv = document.createElement('DIV');
  overlayDiv.style.position = 'absolute';
	
	//Creating an img to be put inside the div
	var img_ = this.imgs_[0];
	var imgInDiv = document.createElement('IMG');
	imgInDiv.src = img_.src_;
	overlayDiv.appendChild(imgInDiv);

	this.div_ = overlayDiv;

  //Adding the div to the right pane
  var panes = this.getPanes();
  panes.overlayLayer.appendChild(overlayDiv);
    
};

PKSOverlay.prototype.draw = function() {
    var overlayProjection = this.getProjection();

    var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());

    var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

    var div = this.div_;
    div.style.left = sw.x + 'px';
    div.style.top = ne.y + 'px';
    div.style.width = (ne.x - sw.x) + 'px';
    div.style.height = (sw.y - ne.y) + 'px';
    div.style.border = 'solid 1px #666';

};

PKSOverlay.prototype.onRemove = function() {
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
};

PKSOverlay.prototype.hide = function() {
    if(this.div_) {
        this.div_.style.visibility = 'hidden';
    }
};

PKSOverlay.prototype.show = function() {
    if(this.div_) {
        this.div_.style.visibility = 'visible';
    }
};


PKSOverlay.prototype.toggle = function() {
    if (this.div_) {
        if (this.div_.style.visibility === 'hidden') {
            this.show();
        } else {
            this.hide();
        }
    }
};

PKSOverlay.prototype.addImage = function(src, index) {
    var img = new Img(src, index);
    this.imgs_.push(img);
};
