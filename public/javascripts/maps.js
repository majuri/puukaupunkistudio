var map;
var infowindow;
var markerimage;
var html;
var overlay;



function initialize() {
    var myLatLng = new google.maps.LatLng(60.170354, 24.82822);
    var myOptions = {
        zoom: 13,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.SATELLITE
    };

    //luodaan kartta
    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

    //luodaan uusi infoikkuna jonka sisältö ja sijainti muutetaan sitten tarpeen mukaan
    infowindow = new google.maps.InfoWindow();

    // TODO vaihda tämä ja muut formit jade-leiskoiksi.
    html =  '<h1>Puukaupunkistudio</h1>';
    infowindow.setContent(html);

    //palautetaan kartta
    return map;
}

//Funktio luo uuden markkerin, antaa sille eventin tiedot ja sijoittaa sen kartalle oikeaa kohtaan.
function placeMarker(project, marker) {
    // tarkistetaan onko projekti olemassa
    if(project.id != null) {
        var location = new google.maps.LatLng(marker.x,marker.y);
        var marker_ = new google.maps.Marker({
            type: marker.type,
            marker: marker,
            position: location,
            map: map,
            id: project.id,
            project: project
        });
    }
    //Kun klikkaa olemassaolevaa markkeria:
    google.maps.event.addListener(marker_, 'click', function(event) {
				$.post('/test',{project: marker_.project}, function(data) {
					viewMarker(marker_, map, data);
				});
				/*
				$.post('/test', {marker : this}, function(data) {
					console.log("moikkeli");
					viewMarker(marker_, map, data)
				});*/
    });
}


google.maps.InfoWindow.prototype.updateContent = function(marker, data) {
    if(marker.id) {
        var html_ = data
/*
        if(marker.type === 'photo') {
            html_ += '<a class="modal" href="javascript:void(0);">' +
                '<img src=' + marker.marker.image + ' class="infowindow" />' +
                '</a>';
        }*/
        infowindow.setContent(html_);
    }
}

//kun jotain markkeria klikkaa sen infoikkunan tulisi avautua
function viewMarker(marker, map, data) {
    if(marker.id) {
        infowindow.updateContent(marker, data);
    } else {
        alert('this does not have an id');
        infowindow.setContent(html);
    }
    infowindow.open(map, marker);
}

function show(project) {
    var swBounds_ = new google.maps.LatLng(project.swBounds[0],project.swBounds[1]);
    var neBounds_ = new google.maps.LatLng(project.neBounds[0],project.neBounds[1]);
    var bounds = new google.maps.LatLngBounds(swBounds_, neBounds_);
    //console.log('map ' + map +  ' & project ' + project.name);
    overlay = new PKSOverlay(bounds, map, project.imgs);
    overlay.show();
    for(var i = 0; i < project.markers.length; i++) {
        placeMarker(project, project.markers[i]);
    }
}
