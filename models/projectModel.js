var img = require('./imgModel');

var imgs = [];
var imgs2 = [];
var imgs_nakyma1 = [];


var markers = [];
var markers2 = [];

imgs[0] = new img.Img('projects/projekti1/havainne/H_01.jpg', 13, [0,0,0,0]);

imgs[1] = new img.Img('projects/projekti1/asema/A_01.jpg', 16, [37,57,20,20]);
imgs[2] = new img.Img('projects/projekti1/asema/A_02.jpg', 16, [58,58,20,20]);
imgs[3] = new img.Img('projects/projekti1/asema/A_03.jpg', 16, [80,80,20,20]);
imgs[4] = new img.Img('projects/projekti1/asema/A_04.jpg', 16, [10,10,20,20]);

imgs_nakyma1[0] = new img.Img('projects/projekti1/nakyma/N_01.jpg', 16, [58,58,33,33]);
imgs_nakyma1[1] = new img.Img('projects/projekti1/nakyma/N_02.jpg', 16, [58,58,33,33]);
imgs_nakyma1[2] = new img.Img('projects/projekti1/nakyma/N_03.jpg', 16, [58,58,33,33]);
imgs_nakyma1[3] = new img.Img('projects/projekti1/nakyma/N_04.jpg', 16, [58,58,33,33]);
imgs_nakyma1[4] = new img.Img('projects/projekti1/nakyma/N_05.jpg', 16, [58,58,33,33]);
imgs_nakyma1[5] = new img.Img('projects/projekti1/nakyma/N_06.jpg', 16, [58,58,33,33]);
imgs_nakyma1[6] = new img.Img('projects/projekti1/nakyma/N_07.jpg', 16, [58,58,33,33]);
imgs_nakyma1[7] = new img.Img('projects/projekti1/nakyma/N_08.jpg', 16, [58,58,33,33]);
imgs_nakyma1[8] = new img.Img('projects/projekti1/nakyma/N_09.jpg', 16, [58,58,33,33]);


markers[0] = {
    x: 60.1675,
    y: 24.8230,
    type: 'photo',
    image: 'projects/project1/katunakyma.jpg'
};

markers[1] = {
    x: 60.172,
    y: 24.8394,
    type: 'norm',
    content: '<p>adeghpjaerphjserphjpseorbhjoparjbhpaserjbhopdebopj<br/>' +
        'ksdjgisdbhpishvbpshnböphböpnhdböpnhzövnözsvnösdfbnzösbnlö<br/>' +
        'ksdjgisdbhpishvbpshnböphböpnhdböpnhzövnözsvnösdfbnzösbnlö<br/>' +
        'ksdjgisdbhpishvbpshnböphböpnhdböpnhzövnözsvnösdfbnzösbnlö<br/>' +
        'ksdjgisdbhpishvbpshnböphböpnhdböpnhzövnözsvnösdfbnzösbnlö</p>'
};

imgs2[0] = new img.Img('projects/project1/pihanakyma.jpg', 13);
imgs2[1] = new img.Img('projects/project1/Suunnitelman_konsepti.jpg', 16);

markers2[0] = {
    x: 60.16755,
    y: 24.82302,
    type: 'photo',
    image: 'projects/project1/katunakyma.jpg'
};

markers2[1] = {
    x: 60.1722,
    y: 24.83941,
    type: 'norm'
};

var projects = [
    {
        id: 1,
        name: 'Mikon suunnitelma',
        imgs: imgs,
        index: 13,
        architecht: 'Mikko',
        swBounds: [60.165354, 24.81822],
        neBounds: [60.172599, 24.839477],
        markers: markers
    },
    {
        id: 2,
        name: 'Matin suunnitelma',
        imgs: imgs2,
        index: 16,
        architecht: 'Matti',
        swBounds: [60.167589, 24.823029],
        neBounds: [60.169329, 24.827331],
        markers: markers2
    }

];

module.exports.all = projects;

module.exports.find = function(id) {
    id = parseInt(id, 10);
    var found = null;
    projectLoop: for (project_index in projects) {
        var project = projects[project_index];
        if (project.id === id) {
            found = project;
            break projectLoop;
        }
    };
    return found;
}

module.exports.images = function(id) {
    id = parseInt(id, 10);
    var found = [];
    imageLoop: for(project_index in projects) {
        var project = projects[project_index];
        if (project.id === id) {
            for(var i = 0; i < project.imgs.length; i++) {
                found.push(project.imgs[i]);
            };
        }
    };
    return found;
}
