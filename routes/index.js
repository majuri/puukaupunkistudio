
/*
 * GET home page.
 */
var projects = require('../models/projectModel');

exports.index = function(req, res){
  res.render('index', { title: 'Puukaupunkistudio', projects : projects.all, project: null })
};

exports.project = function(req, res){
  var project_ = projects.find(req.params.id);
  res.render('index', { title: 'Puukaupunkistudio' , projects : projects.all, project: project_ })
};

exports.test = function(req, res) {
	console.log(req.body);
	console.log("test");
	res.render('partials/marker', {project: req.body.project});
}
